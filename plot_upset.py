import argparse
import csv
import matplotlib.pyplot as plt
import pandas as pd
import pyupset as pyu


def get_signif_assocs(fp, args):
    reader = csv.DictReader(fp, dialect='excel-tab')
    yield reader.fieldnames
    yield from filter(lambda line: float(line[args.key]) < args.alpha, reader)


def plot_upset(args):
    plt.figure(figsize=[10, 10])
    outstats = {}
    for name, fp in zip(args.names, args.infiles):
        fieldnames, *assocs = get_signif_assocs(fp, args)
        outstats[name] = pd.DataFrame(assocs, columns=fieldnames)
    pyu.plot(outstats, unique_keys=['lsv_id', 'snp_id'])
    plt.savefig(args.outfile, transparent=True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('outfile')
    parser.add_argument('infiles', nargs='+', type=argparse.FileType())
    parser.add_argument('--alpha', type=float, default=0.05)
    parser.add_argument('--key', default='linreg_fdr')
    parser.add_argument('--names', nargs='+', required=True)
    args = parser.parse_args()
    assert len(args.names) == len(args.infiles)
    plot_upset(args)


if __name__ == '__main__':
    main()
