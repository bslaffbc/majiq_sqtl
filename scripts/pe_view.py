import argparse
import matplotlib.pyplot as plt
import matplotlib.patches as pch
import numpy as np
import collections
import pysam
import io
from progressbar import ProgressBar


def fetch_pairs(bamfile, *fetch_args, **fetch_kws):
    seen_qnames = {}
    for read in bamfile.fetch(*fetch_args, **fetch_kws):
        mate = seen_qnames.get(read.query_name)
        if mate:
            yield read, mate
            seen_qnames.pop(read.query_name)
        else:
            seen_qnames[read.query_name] = read


# PE VIEW
# Given an alignment BAM file, fetches all fragments mapping to a given
# region which overlap a given exon, counts the number of times a
# fragment starts or ends at each position, and plots the result on a
# backdrop of a gene graph denoting the exons.  No normalization is
# performed by this script whatsoever.


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('bamfile', type=pysam.AlignmentFile)
    parser.add_argument('chromosome')
    parser.add_argument('start', type=int)
    parser.add_argument('end', type=int)
    parser.add_argument('exstart', type=int)
    parser.add_argument('exend', type=int)
    parser.add_argument('bedfile', type=argparse.FileType())
    parser.add_argument('pltfile')
    args = parser.parse_args()

    bamfile: pysam.AlignmentFile = args.bamfile
    chromosome: str = args.chromosome 
    start: int = args.start
    end: int = args.end
    exstart: int = args.exstart
    exend: int = args.exend
    bedfile: io.TextIO = args.bedfile
    pltfile: str = args.pltfile
    resolution = 50

    bedregions = np.genfromtxt(bedfile, delimiter='\t',
                               names=('Chr', 'Start', 'End'),
                               dtype=None)

    pair_cts = collections.Counter

    bar = ProgressBar()

    for read, mate in bar(fetch_pairs(bamfile, contig=chromosome, start=start,
                                      stop=end)):
        positions = (read.reference_start, read.reference_end,
                     mate.reference_start, mate.reference_end)
        left = min(positions)
        right = max(positions)
        if left < start or right > end:
            continue
        if read.get_overlap(exstart, exend) + mate.get_overlap(exstart, exend):
            pair_cts[(left, right)] += 1
    if len(pair_cts) == 0:
        print('no reads found')
        return

    coords = np.array(pair_cts.keys(), dtype=[('3\'', int), ('5\'', int)])
    min_3pr = coords['3\''].min()
    min_5pr = coords['5\''].min()
    max_3pr = coords['3\''].max()
    max_5pr = coords['5\''].max()

    countsmat = np.zeros(((max_3pr - min_3pr) / resolution + 1,
                          (max_5pr - min_5pr) / resolution + 1),
                         dtype=int)
    for ends, count in pair_cts.items():
        _3pr, _5pr = ends
        countsmat[(_3pr - min_3pr) / resolution,
                  (_5pr - min_5pr) / resolution] += count

    ax = plt.figure(figsize=[10, 10]).gca()
    ax_tl = plt.subplot()
    plt.imshow(countsmat, cmap='bwr')
    plt.legend()
    ax.set_xticklabels(range(min_3pr, max_3pr + 1, resolution))
    ax.set_yticklabels(range(min_5pr, max_5pr + 1, resolution))
    for chrom, st, nd in bedregions:
        if chrom != chromosome:
            continue
        left = float(st - min_3pr) / resolution
        bottom = float(st - min_5pr) / resolution
        width = float(nd - st) / resolution
        ax.add_patch(pch.Rectangle((left, -20), width, 20))
        ax.add_patch(pch.Rectangle((-20, bottom), 20, width))
    plt.tight_layout()
    plt.savefig(pltfile, transparent=True)
    plt.close()


if __name__ == '__main__':
    main()
