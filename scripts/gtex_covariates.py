from __future__ import division, print_function
import argparse
import numpy as np
import pysam
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
from src.utils import gtex_convert_name


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('tissue', type=argparse.FileType())
    parser.add_argument('agesex', type=argparse.FileType())
    parser.add_argument('output', type=argparse.FileType('w'))
    parser.add_argument('--vcf', type=pysam.VariantFile)
    parser.add_argument('--config', type=argparse.FileType())
    args = parser.parse_args()

    output = []
    field_names = []
    subj_names = []
    for line in args.tissue:
        if not line.startswith('Inferred'):
            output.append(line.split())
            field_names.append(output[-1][0])
            if field_names[-1] == 'ID':
                subj_names = output[-1][1:]
            else:
                output[-1][1:] = [float(x) for x in output[-1][1:]]
    for line in args.agesex:
        if line.startswith('SUBJID'):
            continue
        name, sex, age, dthhrdy = line.split('\t')
        if age not in field_names:
            field_names.append(age)
            output.append([age] + [1 for _ in subj_names])
        if name in subj_names:
            output[field_names.index(age)][subj_names.index(name) + 1] = 2
    shortnames = list(map(gtex_convert_name, subj_names))
    origsnames = [name for name in shortnames]
    if args.vcf is not None:
        vcfnames = list(map(gtex_convert_name, args.vcf.header.samples))
        shortnames = np.intersect1d(shortnames, vcfnames)
    if args.config is not None:
        config = configparser.ConfigParser()
        config.optionxform = str
        config.readfp(args.config)
        psinames = [gtex_convert_name(name)
                    for name, _ in config.items('psifiles')]
        shortnames = np.intersect1d(shortnames, psinames)
    name_idxs = list(map(origsnames.index, shortnames))

    args.output.write('\t'.join(output[0]) + '\n')
    covars = np.array([row[1:] for row in output[1:]])
    covars = covars[:, name_idxs]
    covdev = covars - covars.mean(axis=1, keepdims=True)
    u, s, vh = np.linalg.svd(covdev, full_matrices=False)
    idxs, = np.where(s >= np.finfo(float).eps * s.size)

    # np.savetxt(args.output, output, fmt='%s', delimiter='\t')
    for idx in idxs:
        row = output[idx + 1]
        args.output.write('%s\t' % row[0])
        args.output.write('\t'.join(map('{:.4g}'.format, row[1:])) + '\n')


if __name__ == '__main__':
    main()
