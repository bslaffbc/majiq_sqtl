"""
Convert Voila PSI files to a matrix format compatible with MatrixEQTL or
FastQTL.
"""

from __future__ import division, print_function

import argparse
import os
import shutil
import numpy as np
import scipy.special as sc
import pysam
import pybedtools
from src.utils import common, gtex_convert_name
from src.voila_io import load_lsvs


def get_psi_vector(lsv, sampnames, logit=False):
    jn_sel = lsv.psi.var(axis=0).argmax()
    output = []
    for name in sampnames:
        name = gtex_convert_name(name)
        if name in lsv.samples:
            output.append(lsv.samples[name][jn_sel])
        else:
            output.append(np.nan)
    return sc.logit(output) if logit else np.array(output)


def get_snp_genotypes(snp, sampnames):
    output = []
    for name in sampnames:
        try:
            output.append('%d' % sum(snp.samples[str(name)].allele_indices))
        except Exception as e:
            output.append('NA')
    return output


def is_in_any_region(lsv, regions):
    if len(regions) == 0:
        return True
    for chrom, start, end in regions:
        if lsv.chrom == chrom:
            if start is None or end is None:
                return True
            if lsv.start == start and lsv.end == end:
                return True
    return False


def main():
    parser = argparse.ArgumentParser(parents=[common])
    parser.add_argument('mode', choices=['fastqtl', 'matrixeqtl'])
    parser.add_argument('output', type=argparse.FileType('w'))
    parser.add_argument('-v', '--vcf', type=pysam.VariantFile, required=True)
    parser.add_argument('--logit', action='store_true')
    parser.add_argument('--covars', type=argparse.FileType())
    parser.add_argument('--region', action='append', nargs=3)
    args = parser.parse_args()

    dirname = os.path.dirname(args.output.name)
    np.set_printoptions(nanstr='NA', infstr='Inf')
    lsvs = load_lsvs(args)
    sampnames = list(args.vcf.header.samples)
    subjnames = [gtex_convert_name(name) for name in sampnames]
    if args.covars:
        line = args.covars.next().split()[1:]
        subjnames = np.intersect1d(line, subjnames)
        assert len(subjnames) > 0
        tsnames = [gtex_convert_name(name) for name in sampnames]
        sampnames = [sampnames[tsnames.index(name)] for name in subjnames]
    if args.mode == 'fastqtl':
        args.output.write('#Chr\tstart\tend\tID\t')
        args.output.write('\t'.join(sampnames))
    elif args.mode == 'matrixeqtl':
        args.output.write('id\t')
        args.output.write('\t'.join(subjnames))
        psinames = [key for key, value in args.config.items('psifiles')]
        sampnames = np.intersect1d(psinames, sampnames)
    args.output.write('\n')
    if args.mode == 'matrixeqtl':
        gntposfile = open(os.path.join(dirname, 'geneloc.txt'), 'w')
        gntposfile.write('geneid\tchr\ts1\ts2\n')
    else:
        gntposfile = None  # suppress IDE warnings
    regions = []
    if args.region is not None:
        for chrom, start, end in args.region:
            try:
                regions.append((chrom, int(start), int(end)))
            except ValueError:
                if start == 'NA' and end == 'NA':
                    regions.append((chrom, None, None))
    for lsv in lsvs.values():
        if not is_in_any_region(lsv, regions):
            continue
        vector = get_psi_vector(lsv, sampnames, logit=args.logit)
        if len(sampnames) - np.isnan(vector).sum() < 9:
            continue
        if np.nanstd(vector) < 1e-2:
            continue
        if args.mode == 'fastqtl':
            args.output.write('%s\t%d\t%d\t%s' % (lsv.chrom,
                                                  lsv.start,
                                                  lsv.end,
                                                  lsv.id))
        elif args.mode == 'matrixeqtl':
            args.output.write('%s' % lsv.id)
            gntposfile.write('%s\t%s\t%d\t%d\n' % (lsv.id,
                                                   lsv.chrom,
                                                   lsv.start,
                                                   lsv.end))
        for x in vector:
            args.output.write('\tNA' if np.isnan(x) else ('\t%.4f' % x))
        args.output.write('\n')

    if args.mode == 'matrixeqtl':
        gntposfile.close()
    print('Wrote PSI output to %s' % args.output.name)

    if args.mode == 'fastqtl':
        args.output.close()
        pybedtools.BedTool(args.output.name).tabix(force=True)

    elif args.mode == 'matrixeqtl':
        print('Formatting %s' % args.vcf.filename)
        with open(os.path.join(dirname, 'SNP.txt'), 'w') as o,\
                open(os.path.join(dirname, 'snpsloc.txt'), 'w') as s:
            o.write('id\t')
            o.write('\t'.join(subjnames))
            o.write('\n')
            s.write('snp\tchr\tpos\n')
            if len(regions) == 0:
                for snp in args.vcf.fetch():
                    if snp.id is None:
                        continue
                    gnt = get_snp_genotypes(snp, sampnames)
                    if gnt.count('0') < 3 or gnt.count('1') < 3 or gnt.count('2') < 3:
                        continue
                    o.write('%s\t' % snp.id)
                    o.write('\t'.join(gnt))
                    o.write('\n')
                    s.write('%s\t%s\t%d\n' % (snp.id, snp.chrom, snp.pos))
            else:
                for chrom, start, end in regions:
                    for snp in args.vcf.fetch(chrom, start, end):
                        if snp.id is None:
                            continue
                        gnt = get_snp_genotypes(snp, sampnames)
                        if gnt.count('0') < 3 or gnt.count('1') < 3 or gnt.count('2') < 3:
                            continue
                        o.write('%s\t' % snp.id)
                        o.write('\n')
                        s.write('%s\t%s\t%d\n' % (snp.id, snp.chrom, snp.pos))
        if args.covars:
            shutil.copy(args.covars.name,
                        os.path.join(dirname, 'Covariates.txt'))

        print('Wrote GNT to %s' % o.name)


if __name__ == '__main__':
    main()
