from __future__ import print_function, division

import argparse
from filter_gff3_common import Gff3


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('gff3', type=Gff3)
    parser.add_argument('output', type=argparse.FileType('w'))
    parser.add_argument('--gene-ids', nargs='+')
    parser.add_argument('--gene-ids-file', type=argparse.FileType())
    parser.add_argument('--disable-progress-bar', dest='bar',
                        action='store_false')
    args = parser.parse_args()

    if args.gene_ids is not None:
        gene_ids = args.gene_ids
    elif args.gene_ids_file is not None:
        gene_ids = [line.strip() for line in args.gene_ids_file]
    else:
        raise argparse.ArgumentError('At least one must be provided: '
                                     '--gene-ids,'
                                     '--gene-ids-file')

    args.gff3.read(gene_ids, bar=args.bar)
    args.gff3.write(args.output)


if __name__ == '__main__':
    main()
