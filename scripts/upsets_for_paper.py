import argparse
import matplotlib.pyplot as plt
import pandas as pd
import upsetplot as pyu
import os
import csv
from collections import defaultdict, Counter


def load_signif_sqtls(
        fname: str,
        key: str = 'gene_name',
        filter_key: str = 'linreg_pval',
        alpha: float = 0.05
):
    output = set()
    with open(fname) as fp:
        reader = csv.DictReader(fp, dialect='excel-tab')
        for row in filter(lambda r: float(r[filter_key]) < alpha, reader):
            output.add(row[key])
    print(fname, len(output))
    return output


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', dest='outfile', required=True)
    parser.add_argument('-i', dest='infiles', nargs=2, action='append', required=True)
    parser.add_argument('-k', dest='key', default='gene_name')
    parser.add_argument('-f', dest='filter_key', default='linreg_pval')
    parser.add_argument('-a', dest='filter_alpha', type=float, default=0.05)
    args = parser.parse_args()
    os.makedirs(os.path.dirname(args.outfile) or '.', exist_ok=True)

    names = tuple(name for name, fname in args.infiles)

    data = defaultdict(lambda: [False for _ in args.infiles])
    for i, (name, fname) in enumerate(args.infiles):
        for value in load_signif_sqtls(
            fname,
            key=args.key,
            filter_key=args.filter_key,
            alpha=args.filter_alpha
        ):
            data[value][i] = True

    by_boolean = Counter()
    for key, index in data.items():
        by_boolean[tuple(index)] += 1

    levels = [[False, True] for i in names]
    labels = list(zip(*by_boolean.keys()))
    print(len(labels), len(labels[0]))

    index = pd.MultiIndex(levels=levels, labels=labels, names=names)
    series = pd.Series(by_boolean, index=index, dtype=int)

    nn = 0
    for i in range(32):
        if i & 0b11100 and i & 0b00011:
            vec = [(i >> j) & 1 == 1 for j in range(5)]
            nn += series.get(tuple(vec), 0)
    print('Shared', nn)

    nn = 0
    for i in range(32):
        if (not i & 0b11100) and i & 0b00011:
            vec = [(i >> j) & 1 == 1 for j in range(5)]
            nn += series.get(tuple(vec), 0)
    print('Unique to SMC/EC', nn)

    fig = plt.figure()
    subplots = pyu.plot(
        series,
        fig,
        sort_by='cardinality'
    )
    print(subplots)

    plt.savefig(args.outfile, transparent=True)


if __name__ == '__main__':
    main()
