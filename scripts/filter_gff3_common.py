from __future__ import print_function, division

import csv
import math
import progressbar
from collections import defaultdict
from functools import total_ordering


@total_ordering
class Gff3Row:
    def __init__(self, seqid=None, type=None, feature=None, start=None,
                 end=None, score=None, strand=None, phase=None,
                 attributes=None):
        self.seqid = seqid
        self.type = type
        self.feature = feature
        self.start = int(start)
        self.end = int(end)
        self.score = float(score) if score != '.' else float('nan')
        self.strand = strand
        self.phase = phase
        self.attrs = {}
        for pair in attributes.split(';'):
            key, value = pair.split('=')
            try:
                value = int(value)
            except ValueError:
                pass
            setattr(self, key, value)
            self.attrs[key] = value
        self.children = []
        self.parent = None

    def __eq__(self, other):
        if self.start == other.start and self.end == other.end:
            if hasattr(self, 'ID') and hasattr(other, 'ID'):
                return self.ID == other.ID
            return True
        return False

    def __lt__(self, other):
        if self.seqid != other.seqid:
            return self.seqid < other.seqid
        return self.start < other.start or self.end < other.end

    def is_parent_of(self, other):
        if hasattr(other, 'ID') and other in self.children:
            return True
        if not hasattr(self, 'ID'):
            return False
        if not hasattr(other, 'Parent'):
            return False
        return self.ID == other.Parent

    def is_child_of(self, other):
        return other.is_parent_of(self)

    def write(self, writer):
        tup = (self.seqid, self.type, self.feature, str(self.start),
               str(self.end),
               str(self.score) if not math.isnan(self.score) else '.',
               self.strand, self.phase,
               ';'.join(['{}={}'.format(key, value)
                         for key, value in self.attrs.items()]))
        writer.writerow(tup)
        for child in self.children:
            child.write(writer)
        return len(self.children) + 1

    def add_child(self, other):
        if self.is_parent_of(other):
            self.children.append(other)
            other.parent = self

    def is_member_of(self, gene_id):
        obj = self
        while obj is not None:
            if obj.ID == gene_id:
                return True
            obj = obj.parent
        return False

    def add_children(self, iterable):
        rest = []
        searchspace = [seg for seg in iterable
                       if seg.start >= self.start
                       and seg.end <= self.end]
        for other in searchspace:
            (self.children if self.is_parent_of(other) else rest).append(other)
        for child in self.children:
            child.add_children(rest)


class Gff3:
    fieldnames = ('seqid', 'type', 'feature', 'start', 'end', 'score',
                  'strand', 'phase', 'attributes')

    def __init__(self, fname):
        self.fname = fname
        self.data = defaultdict(list)
        self.filt = defaultdict(list)

    @staticmethod
    def get_pbar(bar=True):
        def barless(iterable):
            for x in iterable:
                yield x

        return progressbar.ProgressBar() if bar else barless

    def read(self, gene_ids=None, bar=True):
        print('Reading', self.fname)
        with open(self.fname) as fp:
            reader = csv.DictReader(filter(lambda row: not row.startswith('#'),
                                           fp), dialect='excel-tab',
                                    fieldnames=self.fieldnames)
            for row in self.get_pbar(bar)(reader):
                gffrow = Gff3Row(**row)
                if hasattr(gffrow, 'ID'):
                    if gene_ids is not None and gffrow.ID in gene_ids:
                        dest = self.filt
                    else:
                        dest = self.data
                    dest[gffrow.seqid].append(gffrow)
        self.get_children(bar=bar)

    def apply_filter(self, gene_ids, bar=True):
        for seq, genes in self.data.items():
            for gffrow in list(genes):
                if gffrow.ID in gene_ids:
                    genes.remove(gffrow)
                    self.filt[seq].append(gffrow)
        self.get_children(bar=bar)

    def get_children(self, bar=True):
        print('Getting parent-child relationships...')
        # Recursively add children
        for seqid, seqs in self.filt.items():
            if seqid in self.data:
                print(seqid)
                for segx in self.get_pbar(bar)(seqs):
                    segx.add_children(self.data[seqid])

    def write(self, fp):
        fp.write('##gff-version 3\n')
        writer = csv.writer(fp, dialect='excel-tab', lineterminator='\n')
        n_written = 0
        print('Writing to', fp.name)
        for seqid, segs in self.filt.items():
            for seg in segs:
                n_written += seg.write(writer)
        print('Wrote', n_written, 'lines')
