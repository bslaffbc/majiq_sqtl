from __future__ import print_function, division
import argparse
import numpy as np


def get_mapping(kind, fpath, *colidxs):
    kind = kind.lower()
    names = ('lsv_id', 'snp_id', 'pvalue') if kind == 'fastqtl' else True
    arr = np.genfromtxt(fpath, dtype=None, usecols=map(int, colidxs),
                        names=names, encoding=None)
    arr.sort(order=(arr.dtype.names[2],))
    ids = []
    pvals = []
    for lsv_id, snp_id, pvalue in arr:
        ids.append('%s %s' % (lsv_id, snp_id))
        pvals.append(pvalue)
    return np.array(ids), np.array(pvals)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--qtl1', nargs=5, required=True)
    parser.add_argument('--qtl2', nargs=5, required=True)
    parser.add_argument('--alpha', type=float, default=0.05)
    args = parser.parse_args()

    ids1, vals1 = get_mapping(*args.qtl1)
    ids2, vals2 = get_mapping(*args.qtl2)

    n1 = (vals1 < args.alpha).sum()
    n2 = (vals2 < args.alpha).sum()
    nn = min(n1, n2)

    overlap = np.intersect1d(ids1[vals1 < args.alpha],
                             ids2[vals2 < args.alpha])

    print(overlap)


if __name__ == '__main__':
    main()
