import pysam


def fetch_pairs(bamfile, *fetch_args, **fetch_kws):
    seen_qnames = {}
    for read in bamfile.fetch(*fetch_args, **fetch_kws):
        mate = seen_qnames.get(read.query_name)
        if mate:
            yield read, mate
            seen_qnames.pop(read.query_name)
        else:
            seen_qnames[read.query_name] = read


def dict_inc(d, k, default=0):
    d[k] = (d.get(k) or default) + 1
    return d[k]
