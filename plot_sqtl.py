import os
import scipy.stats as st
import scipy.special as sc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as pch
import matplotlib.text as txt
import argparse
import palettable
from voila.api.splice_graph import Exons
from progressbar import ProgressBar
from sklearn.impute import SimpleImputer
from src.utils import makedirs_exist_ok, residualize, load_covars
from src.utils import apply_normalization, common, sqtl_parse
from src.voila_io import load_lsvs, Lsv
from src.vcf_io import load_snps, Snp


def get_junc_color(i):
    junc_colors = palettable.colorbrewer.qualitative.Set1_9.mpl_colors
    r, g, b = junc_colors[i % len(junc_colors)]
    light_scale = i // len(junc_colors)
    if light_scale > 0:
        rdiff = 255 - r
        gdiff = 255 - g
        bdiff = 255 - b
        for i in range(light_scale):
            rdiff *= 0.4
            gdiff *= 0.4
            bdiff *= 0.4
        r = int(255 - rdiff)
        g = int(255 - gdiff)
        b = int(255 - bdiff)
    return r, g, b


def plot_current(row, lsv: Lsv, snp: Snp, covariates, args):
    ref, alt = snp.alleles
    genos = ['{}/{} (N={:d})'.format(*x)
             for x in ((ref, ref, row['num_homo_a']),
                       (ref, alt, row['num_hetero']),
                       (alt, alt, row['num_homo_b']))]
    if args.all_jns:
        jns = range(lsv.psi.shape[1])
    else:
        jns = row['jn_idx'],
    for jn_idx in jns:
        psi = lsv.psi[:, jn_idx]
        psi = SimpleImputer().fit_transform(np.reshape(psi, (-1, 1)))[:, 0]
        psi_orig = psi.copy()
        if args.logit:
            psi = sc.logit(psi)
        psi = apply_normalization(psi, args.normalize)
        if covariates is not None and args.normalize != 'off':
            covars = SimpleImputer().fit_transform(lsv.get_covars(covariates)).T
            psi = residualize(covars, psi)
        gnt = snp.gnt(lsv)
        homo_r = psi[gnt == 0]
        hetero = psi[gnt == 1]
        homo_a = psi[gnt == 2]

        plt.figure()
        nanidxs = np.where([not np.isnan(g) for g in gnt])
        jitter = st.beta.rvs(2, 2, size=nanidxs[0].size, loc=.55, scale=.9)
        boxdict = plt.boxplot([homo_r, hetero, homo_a], vert=True, showmeans=True,
                              showfliers=False)
        scatter = plt.scatter(gnt[nanidxs] + jitter, psi[nanidxs],
                              s=4, c=[get_junc_color(jn_idx)])
        plt.xticks(range(1, 4), genos)
        if args.normalize == 'off':
            normstr = ''
        elif args.normalize == 'standard':
            normstr = 'Normalized '
        else:
            normstr = 'Quantile normalized '
        ylabel = '%s%sJunction %s$\\Psi$' % (normstr,
                                    'Transformed ' if args.logit else '',
                                    'Corrected ' if args.logit and covariates is not None else '')
        plt.ylabel(ylabel)
        if 0 in gnt and 2 in gnt:
            abs_dpsi = abs(psi_orig[gnt == 0].mean() - psi_orig[gnt == 2].mean())
        else:
            abs_dpsi = np.nan
        plt.title('{}:{:d}-{:d}#{:d} vs {}\n'
                  'dist = {:d}, {:s} = {:.4g}\n'
                  '$E[|\\Delta\\Psi|] = {:.4g}$'
                  .format(lsv.name, lsv.start, lsv.end, jn_idx, snp.id,
                          row['distance'], args.field, row[args.field],
                          abs_dpsi))
        plt.ylim(-0.05, 1.05)
        plt.tight_layout()
        ofname = os.path.join(args.outdir, '{}_{}_{}.pdf'.format(lsv.id, snp.id, jn_idx))
        plt.savefig(ofname, transparent=True)
        plt.close()


def plot_splicegraphs(args, lsv, snps):
    plt.figure(figsize=[10, 2])

    plot_min = lsv.start
    plot_max = lsv.end
    # for snp in snps:
    #     plot_min = min(snp.pos, plot_min)
    #     plot_max = max(snp.pos, plot_max)
    for start, end in lsv.exons:
        plot_min = min(plot_min, start)
        plot_max = max(plot_max, end)
    plot_len = plot_max - plot_min + 1
    plot_min -= (plot_len + 2) // 3
    plot_max += (plot_len + 2) // 3

    exon_patches = []
    exon_labels = []
    junction_patches = []
    snp_patches = []
    snp_labels = []

    exons = [exon for exon in args.plot_splicegraph.exons(lsv.gene_id) if -1 not in (exon['start'], exon['end'])]
    if lsv.strand == '-':
        exons = reversed(exons)

    for exon_idx, exon in enumerate(exons):
        start = exon['start']
        end = exon['end']
        if start == lsv.start and end == lsv.end:
            color = '#FFA500'
        elif (start, end) in lsv.exons:
            color = '#808080'
        else:
            color = '#EEEEEE'
        exon_patches.append(
            pch.Rectangle((start, -0.2), end - start, 0.4, facecolor=color, edgecolor=(0, 0, 0), zorder=4))
        exon_labels.append(
            txt.Text((start + end) / 2, 0, f'{exon_idx + 1:d}', color='k', zorder=7, horizontalalignment='center',
                     verticalalignment='center', fontsize=12))

    for jn_idx, (start, end) in enumerate(lsv.junctions):
        junction_patches.append(
            pch.Arc(((start + end) / 2, 0.2), end - start, 1.0, theta1=0, theta2=180, color=get_junc_color(jn_idx),
                    linewidth=2, zorder=3))

    for start, end in lsv.intron_retention:
        junction_patches.append(
            pch.Rectangle((start, -0.05), end - start, 0.1, facecolor=get_junc_color(lsv.njunc), edgecolor=(0, 0, 0), zorder=2))

    for snp in snps:
        snp_patches.append(plt.plot([snp.pos, snp.pos], [-0.1, 0.1], color='k', zorder=5))
        snp_labels.append(
            txt.Text(snp.pos, -0.35, snp.id, color='k', rotation=-45, zorder=6, fontsize=10))

    plt.plot([plot_min, plot_max], [0, 0], color='k', zorder=1)
    plt.axis('off')

    plt.xlim(plot_min, plot_max)
    plt.ylim(-1.2, 1.2)
    plt.tight_layout(0)
    ax: plt.Axes = plt.gca()
    for patch in exon_patches + junction_patches:
        ax.add_patch(patch)
    for text in exon_labels + snp_labels:
        ax.add_artist(text)
    ofname = os.path.join(args.outdir, '{}_splicegraph.pdf'.format(lsv.id))
    plt.savefig(ofname, transparent=True)

    plt.close()


def main():
    parser = argparse.ArgumentParser(parents=[common, sqtl_parse])
    parser.add_argument('pvals', type=argparse.FileType(),
                        help='Path to call_sqtls.py output')
    parser.add_argument('outdir', type=makedirs_exist_ok,
                        help='Directory to which output is to be saved')
    parser.add_argument('--field', default='ftest_pval',
                        help='Field by which to filter on')
    parser.add_argument('--cutoff', type=float, default=0.05)
    parser.add_argument('--all-jns', action='store_true')
    parser.add_argument('--plot-splicegraph', type=Exons)
    args = parser.parse_args()

    stats = np.genfromtxt(args.pvals, dtype=None, names=True, delimiter='\t',
                          encoding=None)
    stats = stats[stats[args.field] <= args.cutoff]
    lsvs = load_lsvs(args, id_filter=stats['lsv_id'])
    snps = load_snps(args, id_filter=stats['snp_id'])
    covariates = load_covars(args) if args.covariates else None

    bar = ProgressBar(max_value=stats.size)
    for row in bar(stats):
        plot_current(row, lsvs[row['lsv_id']], snps[row['snp_id']],
                     covariates, args)
        if args.debug:
            break

    if args.plot_splicegraph is not None:
        for lsv_id in np.unique(stats['lsv_id']):
            plot_splicegraphs(args, lsvs[lsv_id], [snps[row['snp_id']] for row in stats[stats['lsv_id'] == lsv_id]])
            if args.debug:
                break


if __name__ == '__main__':
    main()
