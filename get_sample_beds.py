#!/bin/python
from __future__ import division, print_function

import argparse
import numpy as np
import os
import pybedtools
import pysam
from src.utils import load_config


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('vcffile')
    parser.add_argument('assocfile')
    parser.add_argument('gff3')
    parser.add_argument('outdir',
                        type=lambda d: os.makedirs(d, exist_ok=True) or d)
    parser.add_argument('genome')
    parser.add_argument('config', type=load_config)
    parser.add_argument('bams', nargs='+')
    parser.add_argument('--column', default='linreg_fdr')
    parser.add_argument('--threshold', type=float, default=0.05)
    parser.add_argument('--bgzip', action='store_true')
    parser.add_argument('--keep-intermediates', action='store_true')
    args = parser.parse_args()
    results = np.genfromtxt(args.assocfile, delimiter='\t', names=True,
                            dtype=None, encoding=None)
    names = list(args.config.options('psifiles'))
    for entry in results[results[args.column] < args.threshold]:
        gene_id = entry['lsv_id'].split(':')[0]
        snp_id = entry['snp_id']
        with pysam.TabixFile(args.gff3) as gff3:
            for gene in gff3.fetch(parser=pysam.asGFF3()):
                if gene.ID == gene_id:
                    break
            else:
                print('Could not find gene', gene_id)
                continue
        fetch_kw = {'contig': gene.contig,
                    'start':  gene.start,
                    'end':    gene.end}
        with pysam.VariantFile(args.vcffile) as vcffile:
            for snp in vcffile.fetch(**fetch_kw):
                if snp.id == snp_id:
                    break
            else:
                print('Could not find snp', snp_id)
                continue
        gts = [[] for ct in range(3)]
        gtstrs = '{0}_{0} {0}_{1} {1}_{1}'.format(*snp.alleles).split()
        for name, sample in snp.samples.items():
            gts[sum(sample.allele_indices)].append(name)
        for ct, gt in enumerate(gts):
            name = np.random.choice(np.intersect1d(gt, names))
            srr = args.config.get('psifiles', name).split('.')[0]
            for bamfile in args.bams:
                if srr in bamfile:
                    break
            else:
                print('could not find BAM file', srr)
                continue
            tname = '{}.{:d}.{}.{}.{}'.format(snp_id,
                                              ct,
                                              gtstrs[ct],
                                              entry['lsv_id'],
                                              name)
            ofbase = '{}.bam'.format(tname)
            ofname = os.path.join(args.outdir, ofbase)
            os.makedirs(os.path.join(args.outdir, tname), exist_ok=True)

            print('Selecting reads for', ofname)
            with pysam.AlignmentFile(bamfile, 'r') as inbam:
                with pysam.AlignmentFile(ofname, 'wb', template=inbam) as outbam:
                    for read in inbam.fetch(**fetch_kw):
                        outbam.write(read)
                    trackopts = 'name="{}" ' \
                                'visibility=2 ' \
                                'db=hg19'.format(tname)

            print('Computing genome coverage for', ofname)
            bedtool = pybedtools.BedTool(ofname)
            bedgraph = bedtool.genome_coverage(bg=True, split=True,
                                               trackline=True,
                                               trackopts=trackopts,
                                               g=args.genome)
            bedgraph = bedgraph.saveas(ofname.replace('.bam', '.bedgraph'))
            if not args.keep_intermediates:
                os.remove(ofname)
            if args.bgzip:
                bedgraph.bgzip(force=True)
                if not args.keep_intermediates:
                    os.remove(ofname.replace('.bam', '.bedgraph'))


if __name__ == '__main__':
    main()
