def get_dtype(args):
    """Auxiliary function to get the NUMPY data type and format spec for the
    TSV output.

    Input: args is the namespace derived from the argument parser in main
    Output:
        fmt = Format spec passed to numpy.savetxt
        dtype = numpy.dtype template passed to the array constructor
    """

    # Initialize dtype to account for the summary data identifying each LSV-
    # SNP pair.
    # We use "object" to account for the varible length of strings in writing
    # the TSV
    dtype = [('chromosome', object),
             ('ref_start', int),
             ('ref_end', int),
             ('lsv_id', object),
             ('gene_name', object),
             ('jn_idx', int),
             ('snp_id', object),
             ('distance', int),
             ('num_homo_a', int),
             ('epsi_homo_a', float),
             ('num_hetero', int),
             ('epsi_hetero', float),
             ('num_homo_b', int),
             ('epsi_homo_b', float)]
    # Tack on the columns for each test statistic
    for stat in args.stats:
        # Extend dtype to account for the output column names
        dtype += [(name, float) for name in stat.outnames]
    # In case the definition of dtype changes in the future, the automatic
    # computation of fmt will allow the code to not crash
    fmt = '\t'.join(map({object: '%s', int: '%d', float: '%g'}.get,
                        (x[1] for x in dtype)))
    return fmt, dtype
