import scipy.stats as st
import numpy as np
from sklearn.impute import SimpleImputer
from src.stats import Statistic
from src.utils import residualize, apply_normalization


class Linreg(Statistic):
    name = 'linreg'
    dirn = False
    outnames = ['slope', 'intercept', 'linreg_stat', 'linreg_pval',
                'linreg_fdr']
    offsets = [4]
    null = [0, 0, 0, 1]

    def __call__(self, labels, values, normalize='off', covariates=None):
        labels = SimpleImputer().fit_transform(np.reshape(labels, (-1, 1)))[:, 0]
        labels = apply_normalization(labels, normalize)
        if covariates is not None:
            labels = residualize(covariates, labels)
        return list(st.linregress(labels, values)[:-1])
