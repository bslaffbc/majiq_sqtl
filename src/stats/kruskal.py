import scipy.stats as st
from src.stats import Statistic


class Kruskal(Statistic):
    name = 'kruskal'
    dirn = False
    outnames = ['kruskal_stat', 'kruskal_pval', 'kruskal_fdr']
    offsets = [2]
    null = [0, 1]

    def __call__(self, labels, values, **kwargs):
        return self.discrete(st.kruskal, *self.get_groups(labels, values))
