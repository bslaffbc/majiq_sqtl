import os
import glob
import numpy as np
import scipy.special as sc
from sklearn.impute import SimpleImputer
from src.utils import residualize, apply_normalization


class Statistic:
    """All test statistics must subclass this."""
    name = ''
    dirn = None
    outnames = []
    offsets = []
    null = []

    def __call__(self, labels, values, normalize='off', covariates=None):
        raise ValueError('__call__ method was not implemented in the '
                         'subclass, or the base class was invoked directly.')

    def __eq__(self, other):
        if isinstance(other, Statistic):
            return self.name == other.name
        if isinstance(other, str):
            return self.name == other
        return NotImplemented

    def __repr__(self):
        return repr(self.name)

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

    @staticmethod
    def get_groups(labels, values):
        for label in set(labels):
            if not np.isnan(label):
                yield values[labels == label]

    def discrete(self, func, *groups):
        """Call the statistical function.  If NaNs are encountered, return
        a p-value of 1."""
        statistic, pvalue = func(*groups)
        if np.isnan((statistic, pvalue)).any():
            return self.null[-2:]
        return [statistic, pvalue]


# Load and initialize the test statistic classes
statistics = {}
for fname in glob.glob(os.path.dirname(__file__) + '/*.py'):
    if os.path.isfile(fname) and '__init__' not in fname:
        fname, _ = os.path.splitext(os.path.basename(fname))
        modname = 'src.stats.{}'.format(fname)
        clsname = fname.title()
        cls = getattr(__import__(modname, fromlist=clsname), clsname)
        statistics[fname] = cls()


def test_current_snp(labels, values, which_stats, logit=False,
                     normalize='off', covariates=None):
    """Perform the actual association tests for the current SNP-junction
    pair.

    Arguments:
        labels: the vector of allele counts per subject
        values: the vector of PSI values
        which_stats: the vector of stat indices
        logit: if True, will impose a logit transform on PSI
        normalize (str): selects the normalization strategy.
            off (default): do not normalize
            standard: shift and scale to mean 0 and variance 1
            quantile: quantile-normalize to a Gaussian with mean 0 and variance
                1
        covariates: if not None, is an array of global covariates

    Yields:
        vector of test statistics and nominal p-values
    """

    # Logit transformation, if requested
    if logit:
        values = sc.logit(values)

    # PSI normalization as requested
    values = apply_normalization(values, normalize)

    # Impute covariates and regress them out
    if covariates is not None:
        covariates = SimpleImputer().fit_transform(covariates).T
        values = residualize(covariates, values)

    # Call the test statistic
    for stat in which_stats:
        yield stat(labels, values, normalize=normalize,
                     covariates=covariates)
