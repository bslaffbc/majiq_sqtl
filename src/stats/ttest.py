import scipy.stats as st
from src.stats import Statistic


class Ttest(Statistic):
    name = 'ttest'
    dirn = False
    outnames = ['ttest_aa_ab_stat', 'ttest_aa_ab_pval', 'ttest_aa_ab_fdr',
                'ttest_aa_bb_stat', 'ttest_aa_bb_pval', 'ttest_aa_bb_fdr',
                'ttest_ab_bb_stat', 'ttest_ab_bb_pval', 'ttest_ab_bb_fdr']
    offsets = [2, 5, 8]
    null = [0, 1, 0, 1, 0, 1]

    def __call__(self, labels, values, **kwargs):
        groups = list(self.get_groups(labels, values))
        results = []
        stat_idxs = []
        for i1, g1 in enumerate(groups[:-1]):
            for g2 in groups[(i1 + 1):]:
                results += self.discrete(st.ttest_ind, g1, g2)
        return results
