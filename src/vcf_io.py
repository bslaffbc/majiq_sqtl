import numpy as np
import pysam
from src.utils import gtex_convert_name
from functools import total_ordering


def dbsnp_convert_contig(chrname):
    if chrname.startswith('chr'):
        chrname = chrname[3:]
    if chrname in ('X', 'Y'):
        return f'NC_{23 if chrname == "X" else 24}.10'
    return f'NC_{int(chrname):06d}.10'


@total_ordering
class Snp:
    """Class that handles information storage and retrieval pertinent to SNPs.

    Constructor arguments:
        ref_object is a pysam.VariantRecord.  See Pysam documentation for more.
    """
    def __init__(self, ref_object):
        self.id = ref_object.id
        self.chrom = ref_object.contig
        self.pos = ref_object.pos
        self.samples = {}
        self.dist = np.inf
        self.alleles = ref_object.alleles
        for subjname, record in ref_object.samples.items():
            subjname = gtex_convert_name(subjname)
            try:
                self.samples[subjname] = sum(record.allele_indices)
            except (TypeError, ValueError):
                self.samples[subjname] = np.nan

    def __eq__(self, other):
        return self.id == other.id

    def __lt__(self, other):
        return self.id < other.id

    def get(self, name):
        """Get current sample by name.  Returns allele pair tuple."""
        return self.samples.get(name, np.nan)

    def gnt(self, lsv):
        """Get the array of genotypes for each sample quantified in the
        passed LSV object.  Each element is an allele pair tuple.
        """
        return np.array([self.get(name) for name in lsv.samples])


def dbsnp_convert_snp(snp, dbsnp=None):
    if dbsnp is not None:
        try:
            refsnp = next(s for s in dbsnp.fetch(dbsnp_convert_contig(snp.contig), snp.pos - 1, snp.pos + 1) if s.pos == snp.pos)
            snp.id = refsnp.id
            del refsnp
        except (StopIteration, AttributeError):
            pass
    return snp


def load_snps(args, id_filter=None, **fetch_kwargs):
    """Load SNPs from the vcf file passed to the top script"""
    snps = {}
    dbsnp: pysam.VariantFile = getattr(args, 'dbsnp', None)
    for snp in args.vcf.fetch(**fetch_kwargs):  # type: pysam.VariantRecord
        snp = dbsnp_convert_snp(snp, dbsnp=dbsnp)
        if id_filter is None or snp.id in id_filter:
            snps[snp.id] = Snp(snp)
    return snps
