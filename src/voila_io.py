import csv
import os
import collections
import numpy as np
import re
import traceback
from src.utils import gtex_convert_name, log
from src.stats import test_current_snp
from src.vcf_io import load_snps

import pickle


def try_int(x):
    try:
        return int(x)
    except ValueError:
        return np.nan


class Lsv:
    """Container object for LSV data.  The final point of divergence for
    multiple version support.

    Constructor args:
        ref_object is either a VoilaLsv, _Psi, or dict instance with the
        necessary information to perform sQTL associations.

    Properties:
        samples is a dictionary of subject PSI vectors
        colors is an internal mapping of LSV junctions to colors, for the
            purposes of plotting
        snps is a list of nearby SNPs
        exons is a list of exon coordinates
        junctions is a list of junction coordinates
        psi (read-only) is a vector of PSI values. This is generated once.
            To reset, call the clear_psi method.
        sel_jn is the selected junction index for sQTL analysis.  When
            uninitalized, this value is -1.
        id is the LSV ID as reported by MAJIQ
        gene_id is the ID of the associated gene locus
        name is the name of the associated gene locus
        type is the internal LSV type as reported by MAJIQ
        chrom is the name of the chromosome the LSV lies on
        start and end are the coordinates of the reference exon
        njunc is the number of junctions
        dirn is True if the LSV split point is at the 5' end of the + strand
            (3' end of the - strand), else False
        cycler (rw) is a Matplotlib cycler which loops through the junction
            colors
    """
    def __init__(self, ref_object):
        self.samples = {}
        self.colors = {}
        self.snps = None
        self._psi = None
        self.sel_jn = None
        self._cycler = None
        self.id = ref_object['LSV ID']
        self.name = ref_object['Gene Name']
        self.type = ref_object['LSV Type']
        self.chrom = ref_object['chr']
        lsv_id_comps = self.id.split(':')
        lsv_id_comps = [':'.join(lsv_id_comps[:-2])] + lsv_id_comps[-2:]
        self.start, self.end = map(try_int, lsv_id_comps[2].split('-'))
        is_target = lsv_id_comps[1] == 't'
        self.exons = ref_object['Exons coords']
        self.junctions = ref_object['Junctions coords']
        self.intron_retention = ref_object['IR coords']
        self.njunc = len(self.junctions)
        self.strand = ref_object['strand']
        if self.strand == '+' and not is_target:
            self.dirn = True
        elif self.strand == '-' and is_target:
            self.dirn = True
        else:
            self.dirn = False

    @property
    def gene_id(self):
        """ID of the associated gene"""
        return self.id.split(':')[0]

    @property
    def cycler(self):
        """Get the next color"""
        return self._cycler.next()['color']

    @cycler.setter
    def cycler(self, cycler):
        """Set the color cycler"""
        self._cycler = cycler

    def color(self, jn):
        """Get the color associated with the given junction index"""
        color = self.colors.get(jn)
        if color is None:
            color = self.colors[jn] = self.cycler
        return color

    def add_sample(self, name, data):
        """Add a subject PSI data

        name is the name of the subject
        data is the LSV or TSV object containing the PSI data
        """
        ms = data['E(PSI) per LSV junction']
        if self.njunc <= 2:
            ms.pop(-1)
        self.samples[name] = ms

    def junction_select(self, args):
        """Perform junction selection if PSI difference filtering is requested.
        For each junction:
            a. Sorts PSI
            b. Removes the gap_select_k smallest and largest values
            c. Determines whether any two adjacent PSI values exceeds min_gap

        Args:
            args is a Namespace object with these attributes:
                gap_select_k is the number of extreme PSI values to discard
                min_gap is the minimum delta PSI difference between any two
                    adjacent PSI values.

        Returns:
            Index of junction with the best gap.  If all junctions would be
                discarded, returns -1.
        """
        k = args.gap_select_k
        min_gap = args.min_gap
        gaps = []
        if len(self.samples) < 2 * k + 1:
            best_jn = -1
        else:
            for jn_idx in range(self.njunc):
                psi = sorted(
                    [val[jn_idx] for val in self.samples.values()])
                gaps.append(np.diff(psi[k:-k]).max())
            best_jn = np.argmax(gaps)
            log.debug('%s %d %s', self.id, best_jn, gaps[best_jn])
            if gaps[best_jn] < min_gap:
                best_jn = -1
        self.sel_jn = best_jn
        return best_jn

    @staticmethod
    def merge_overlapping_regions(regions):
        pass

    def get_regions(self, snp_range, extend_exons=False):
        """Get the regions to search for SNPs in.
        Look {snp_range} positions away from each splice site into the
        intron, and anywhere in the relevant exon bodies.
        Yields left and right endpoints for search."""
        # Check to see whether the reference exon is on the left or right
        # of the splice graph.
        # Reference view has the annotated + strand with the 5' end on the
        # left.
        left_extn = snp_range if self.dirn else 0
        right_extn = 0 if self.dirn else snp_range

        # Yield the reference exon
        start, end = sorted((self.start, self.end))
        yield start - right_extn, end + left_extn, not self.dirn

        # Yield remaining exons
        if extend_exons:
            for estart, eend in map(sorted, self.exons):
                if (estart, eend) != (start, end):
                    yield estart - left_extn, eend + right_extn, self.dirn

    def find_cis_snps(self, args, gwas=None):
        """Finds all putative cis-QTLs using some user-provided heuristics.
        Fills them into self.snps

        Arguments:
            args is a Namespace object with the following attributes:
                vcf is a pysam.VariantFile object. See Pysam docs for more.
                extend_exons is a boolean flag.  If True, will look around all
                    exons associated with the LSV.
                    Otherwise, only looks at the reference exon.
                snp_range is the distance away from each exon to search.
        """
        self.snps = {}
        extend_exons = args.extend_exons
        snp_range = args.snp_range

        # Get the chromosme ID
        if args.contig_override and args.config.has_section('contigs'):
            contig = args.config.get('contigs', self.chrom)
        else:
            contig = self.chrom
        # Sanity check to make sure the VCF and Voila file agree.
        if contig not in args.vcf.header.contigs:
            return

        # Loop over each region to be mapped
        for start, end, side in self.get_regions(snp_range,
                                           extend_exons=extend_exons):
            # Fetch SNPs, building a Snp class
            for snp in load_snps(args, contig=contig, start=start - snp_range, stop=end + snp_range).values():
                # Some SNP IDs are interpreted as None. We don't want to test
                # these because we cannot look them up later.
                if snp.id is None:
                    continue
                # Make sure we're not double-counting SNPs
                if snp.id in self.snps:
                    snp = self.snps[snp.id]
                # GWAS filter
                elif gwas is not None and snp.id not in gwas:
                    continue
                # Get the distance from the splice site.
                ref = start + snp_range if side else end - snp_range
                snp.dist = min(snp.dist, abs(snp.pos - ref))
                self.snps[snp.id] = snp

    @property
    def psi(self):
        """Prefetch the PSI vector."""
        if self._psi is None:
            self._psi = np.array(list(self.samples.values()))
        return self._psi

    def clear_psi(self):
        self._psi = None

    def get_covars(self, covariates=None):
        """Get the array of covariates corresponding with the set of
        quantifiable subjects.

        covariates, if not None, is a dictionary of subject-vector pairs.
        Returns the array of covariates for all the quantifiable subjects.
        Fills with NaN any subject missing covariate data.
        """
        if covariates is not None:
            ncovars = len(list(covariates.values())[0])
            covars = []
            for name, mean in self.samples.items():
                covars.append(np.array(covariates.get(name,
                                                      [np.nan] * ncovars)))
            return np.array(covars)

    def test_snps(self, args, covariates=None, gwas=None):
        """The workhorse of the tool, this routine does all the association
        tests requested by the user."""
        which_stats = args.stats
        logit = args.logit
        normalize = args.normalize
        # min_psi_difference = args.min_delta_psi
        min_mac = args.min_mac
        min_maf = args.min_maf

        psi = self.psi
        # Require a minimum number of quantifiable samples to consider the LSV
        # at all.
        if len(psi) < 9:
            return  # Not enough samples to have at least 3 per genotype class

        # Fetch covariates
        covars = self.get_covars(covariates=covariates)

        # Fetch SNPs.
        if self.snps is None:
            self.find_cis_snps(args, gwas=gwas)

        for snp in self.snps.values():
            # Make sure it's close enough to any splice site.
            dist = snp.dist
            if dist > args.snp_range:
                log.debug(f'{snp.id} too far from {self.id}')
                continue

            # Fetch genotype
            gnt = snp.gnt(self)
            if gnt.size == 0:  # Hopefully this sanity check never trips.
                log.debug(f'{snp.id} genotype sanity check failed for {self.id}')
                continue

            # Check minor allele count
            acnt = (gnt == 0).sum()
            hetcnt = (gnt == 1).sum()
            bcnt = (gnt == 2).sum()

            # Hardcoded requirement to make sure we have enough samples in
            # each genotype group to do inference
            if any([c < 2 for c in (acnt, hetcnt, bcnt)]):
                log.debug(f'{snp.id} not enough samples to declare significance at {self.id}')
                continue
            # Minor allele count is the heterozygote plus twice the homozygote.
            mac = (2 * min(acnt, bcnt) + hetcnt)
            if mac < min_mac:
                log.debug(f'{snp.id} MAC check failed for {self.id}')
                continue
            # Minor allele frequency is minor allele count scaled by subject
            # count
            maf = mac * 0.5 / gnt.size
            if maf < min_maf:
                log.debug(f'{snp.id} MAF check failed for {self.id}')
                continue

            # Get the group means
            psimeans = [psi[gnt == gn_i].mean(axis=0)
                        if gn_i in gnt
                        else np.array(
                [np.nan for _ in range(psi.shape[1])])
                        for gn_i in range(3)]
            # Make sure there's enough non-NaN means to report.
            psimeans_nonempty = [x for x in psimeans if
                                 not np.isnan(x).any()]
            if len(psimeans_nonempty) < 2:
                log.debug(f'{snp.id} not enough non-missing values for {self.id}')
                continue

            # Perform junction selection according to the user-specified
            # strategy.
            if self.sel_jn is None:
                if args.jn_strat == 'maxdpsi':
                    # Use the junction with the greatest absolute difference in
                    # means between homozygotes
                    self.sel_jn = [np.argmax(abs(psimeans_nonempty[-1]
                                                 - psimeans_nonempty[0]))]
                elif args.jn_strat == 'maxtss':
                    # Use the junction with the most underlying PSI variance
                    # before any corrections are applied.
                    self.sel_jn = [np.argmax(np.var(psi, axis=1))]
                elif args.jn_strat == 'selall':
                    self.sel_jn = range(psi.shape[1])

            for jn in self.sel_jn:
                my_psimeans = [cat[jn] for cat in psimeans]

                # # Discard LSVs with PSI not changing enough between groups
                # if abs(psimeans[-1] - psimeans[0]) < min_psi_difference:
                #     continue

                # # Disallow non-monotonic PSI
                # if not (psimeans[0] <= psimeans[1] <= psimeans[2] or
                #         psimeans[2] <= psimeans[1] <= psimeans[0]):
                #     continue

                # Do actual tests
                summary = [dist]
                for gn_i in range(3):
                    summary += [(gnt == gn_i).sum(), my_psimeans[gn_i]]
                stats = test_current_snp(gnt, psi[:, jn], which_stats,
                                         logit=logit,
                                         normalize=normalize,
                                         covariates=covars)
                yield snp, jn, summary, list(stats)


class VoilaLsvProxy(collections.OrderedDict):
    @staticmethod
    def convert_key(key):
        return re.sub(r'\W+', '_', key.lower())

    def __init__(self, mapping):
        super().__init__(mapping)
        for key, value in mapping.items():
            setattr(self, self.convert_key(key), value)


class VoilaReader:
    """A wrapper class that handles CSV reading for the Voila TSV"""
    def __init__(self, fname, **kwargs):
        self.fname = fname
        self.kwargs = kwargs
        self.file = None
        self.reader: csv.DictReader = None

    @staticmethod
    def try_int(xx):
        if xx.lower().startswith('na'):
            return -1
        try:
            return int(xx)
        except ValueError:
            return xx

    @staticmethod
    def try_float(xx):
        try:
            return float(xx)
        except ValueError:
            return xx

    def __enter__(self):
        self.file = open(self.fname)
        self.reader = csv.DictReader(self.file, **self.kwargs)
        self.reader.fieldnames[0] = self.reader.fieldnames[0].lstrip('#')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.__exit__(exc_type, exc_val, exc_tb)
        self.reader = None
        self.file = None

    def __iter__(self):
        if self.reader is None:
            raise TypeError('reader has not been initialized')
        for row in self.reader:
            for key, value in row.items():
                row[key] = value.split(';')
                for i, val in enumerate(row[key]):
                    row[key][i] = self.try_float(val)
                    if isinstance(row[key][i], str) and '-' in val:
                        tmp = tuple(self.try_int(xx) for xx in val.split('-'))
                        if not any(isinstance(x, str) for x in tmp):
                            row[key][i] = tmp
                if len(row[key]) == 1 and isinstance(row[key][0], str):
                    row[key] = row[key][0]
                elif len(row[key]) == 0:
                    row[key] = None
            yield VoilaLsvProxy(row)

    def __getattr__(self, item, default=None):
        if self.reader is None:
            raise TypeError('reader has not been initialized')
        return getattr(self.reader, item, default=default)


def _load_lsvs(args, id_filter=None):
    """Read LSVs from TSV or Voila files."""
    enable_gap_filtering = getattr(args, 'enable_gap_filtering', False)
    lsvs = {}
    prefix = args.config.get('info', 'psidir')
    for name, psifname in args.config.items('psifiles'):
        name = gtex_convert_name(name)
        log.info('LOAD LSVS %s FROM %s', name, psifname)
        absname = os.path.join(prefix, psifname)
        with VoilaReader(absname, dialect='excel-tab') as voila:
            for lsv_obj in voila:
                if id_filter is None or lsv_obj.lsv_id in id_filter:
                    if lsv_obj.lsv_id not in lsvs:
                        lsvs[lsv_obj.lsv_id] = Lsv(lsv_obj)
                    lsvs[lsv_obj.lsv_id].add_sample(name, lsv_obj)
                del lsv_obj
    lsvs = {lsv_id: lsv_obj for lsv_id, lsv_obj in lsvs.items()
            if not any(-1 in sg for sg in lsv_obj.junctions + lsv_obj.exons)}
    if enable_gap_filtering:
        lsvs = {lsv_id: lsv_obj for lsv_id, lsv_obj in lsvs.items()
                if lsv_obj.junction_select(args) != -1}
        assert len(lsvs) > 0, 'NO LSVS SURVIVED JUNCTION SELECTION'
    return lsvs


def load_lsvs(args, id_filter=None):
    """Read LSV objects from Voila or TSV files, or restore from a preloaded
    database if one is supplied."""
    lsvs = None

    if hasattr(args, 'pre_lsv') and args.pre_lsv is not None and \
            os.path.exists(args.pre_lsv) and \
            not args.refresh_db:
        log.info('LOAD LSVS PREPROCESSED FROM %s', args.pre_lsv)
        try:
            with open(args.pre_lsv, 'rb') as fp:
                lsvs = pickle.load(fp)
        except Exception as e:
            log.error('Error loading from %s', args.pre_lsv)
            log.error(traceback.format_exc())

    if lsvs is None:
        lsvs = _load_lsvs(args, id_filter=id_filter)
        if hasattr(args, 'pre_lsv') and args.pre_lsv:
            with open(args.pre_lsv, 'wb') as fp:
                pickle.dump(lsvs, fp)
    return lsvs
