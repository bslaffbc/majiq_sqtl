import argparse
import csv
import logging
import os
import sys
import pysam
import numpy as np
import scipy.stats as st
from src.constants import PKGNAME
import configparser


# Set up the log
log = logging.Logger(PKGNAME)
logfmt = '%(asctime)s (PID:%(process)s) - %(levelname)s - %(message)s'
fmt = logging.Formatter(logfmt)
shandler = logging.StreamHandler()
shandler.setFormatter(fmt)
log.addHandler(shandler)


def register_file_handler(fname):
    """Register the log file handler"""
    fhandler = logging.FileHandler(fname)
    fhandler.setFormatter(fmt)
    log.addHandler(fhandler)


def gtex_convert_name(name):
    """Get the subject name from the run ID"""
    return '-'.join(name.split('-')[:2])


def makedirs_exist_ok(path, mode=0o777):
    """Multiversion support for os.makedirs behavior"""
    if path:
        if sys.version_info >= (3, 2):
            os.makedirs(path, mode=mode, exist_ok=True)
        elif not os.path.exists(path):
            os.makedirs(path, mode=mode)
    return path


def load_config(fname):
    """Load the configuration file and perform sanity checks"""
    config = configparser.ConfigParser()
    config.optionxform = str
    config.read(fname)
    assert config.has_section('info')
    assert config.has_section('psifiles')
    assert config.has_option('info', 'psidir')
    psidir = config.get('info', 'psidir')
    all_exist = True
    for key, basename in config.items('psifiles'):
        if not os.path.exists(os.path.join(psidir, basename)):
            log.error('missing psi file for %s', key)
            all_exist = False
    assert all_exist
    return config


def apply_normalization(vector, strategy='off'):
    """Normalize allele counts or PSI vector according to the given strategy"""
    if strategy == 'quantile':
        xvals = np.linspace(0., 1., num=vector.size + 1) + 0.5 / vector.size
        return st.norm.ppf(xvals[:-1])[vector.argsort().argsort()]
    if strategy == 'standard':
        return st.zscore(vector)
    return vector


def residualize(covars, yvec):
    """Regress out each row of covariates from yvec"""
    if yvec.var() != 0:
        for xvec in covars:
            if xvec.var() != 0:
                slp, itc, rho, pvl, err = st.linregress(xvec, yvec)
                yvec -= slp * xvec
    return yvec


def load_covars(args):
    """Load the covariates matrix"""
    if args.covariates:
        covarf = args.covariates
        log.info('LOAD COVARIATES: %s', covarf.name)
        reader = csv.DictReader(covarf, dialect='excel-tab')
        for i, field in enumerate(reader.fieldnames[1:]):
            reader.fieldnames[i + 1] = gtex_convert_name(field)
        covariates = {name: [] for name in reader.fieldnames[1:]}
        for row in reader:
            row.pop(reader.fieldnames[0])
            for name, value in row.items():
                covariates[name].append(float(value))
        covarf.close()
        return covariates


def load_gwas(args):
    """Read the list of SNPs to filter by"""
    if args.gwas:
        gwasf = args.gwas
        min_ld = args.min_ld
        log.info('LOAD GWAS LD: %s', gwasf.name)
        snps = set()
        reader = csv.DictReader(gwasf, delimiter=' ', skipinitialspace=True,
                                lineterminator='\n')
        for line in reader:
            if float(line['R2']) >= min_ld:
                snps = snps.union([line['SNP_A'], line['SNP_B']])
        return snps


common = argparse.ArgumentParser(add_help=False)
common.add_argument('config', type=load_config,
                    help='Configuration pointing to the PSI files and '
                         'other arguments')
common.add_argument('--pre-lsv',
                    help='Set to a file path where preformatted LSVs '
                         'are to be stored.  Creates this file if it '
                         'does not exist.')
common.add_argument('--refresh-db', action='store_true',
                    help='If set, will force preprocessing of the LSVs '
                         'from scratch as though the file passed to '
                         '--pre-lsv does not exist.')
common.add_argument('--log',
                    help='File to which the logger output should be written')
common.add_argument('--debug', action='store_true',
                    help='Extra debug log.infos')

sqtl_parse = argparse.ArgumentParser(add_help=False)

sqtl_parse.add_argument('-v', '--vcf', type=pysam.VariantFile, required=True,
                        help='Path to VCF or BCF file containing sample '
                             'genotypes.')
sqtl_parse.add_argument('-c', '--covariates', type=argparse.FileType(),
                        help='Path to tab-delimited covariates file. Each '
                             'column represents a subject, and each row '
                             'represents a quantitative covariate.')
sqtl_parse.add_argument('--logit', action='store_true',
                        help='If set, will logit transform PSI.')
sqtl_parse.add_argument('--normalize', choices=['off', 'standard', 'quantile'],
                        default='off',
                        help='Normalization strategy. '
                             'Choices: %(choices)s. '
                             'Default: %(default)s')
sqtl_parse.add_argument('--dbsnp', type=pysam.VariantFile,
                        help='Reference VCF for dbSNP IDs')
