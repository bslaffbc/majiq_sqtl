# MAJIQtl: Splicing QTL analysis using MAJIQ PSI estimates

This repository requires Python 3.6 or higher.  Any shell command reference
to `python` assumes that `python` is an alias for the appropriate version of
Python.  If that is not the case, you will need to specify which Python
executable to use as the interpreter e.g. `python3`, `python3.6`, or an 
explicit path to the Python binary.

## Setting up

Follow the instructions to install MAJIQ (version 2.0).

Then run the following command:

    python -m pip install -r requirements.txt

The main script is `call_sqtls.py`.  It requires a config file which must 
include at least these two containers: `info` and `psifiles`.  A third optional
container, `contigs`, may be supplied.  These sections will be explained later
in this document.

## Calling MAJIQtl from the command line

    python call_sqtls.py [-h] -v VCF [-c COVARIATES] -s
                         {ftest,ttest,correl,tnom,kruskal}
                         [{ftest,ttest,correl,tnom,kruskal} ...] [-j NTHREADS]
                         [-r SNP_RANGE] [-a ALPHA] [-p NPERMUTE] [-g MIN_GAP]
                         [-d MIN_DELTA_PSI] [-k GAP_SELECT_K] [--extend-exons]
                         [--logit] [--normalize {off,standard,quantile}]
                         [--two-stage] [--enable-gap-filtering] [--gwas GWAS]
                         [--min-ld MIN_LD] [--pre-lsv PRE_LSV] [--contig-override]
                         [--debug]
                         config output
    
    positional arguments:
      config                Configuration pointing to the PSI files and other
                            arguments
      output                Output file
    
    optional arguments:
      -h, --help            show this help message and exit
      -v VCF, --vcf VCF     Path to VCF or BCF file containing sample genotypes.
      -c COVARIATES, --covariates COVARIATES
                            Path to tab-delimited covariates file. Each column
                            represents a subject, and each row represents a
                            quantitative covariate.
      -s {ftest,ttest,correl,tnom,kruskal} [{ftest,ttest,correl,tnom,kruskal} ...], --stats {ftest,ttest,correl,tnom,kruskal} [{ftest,ttest,correl,tnom,kruskal} ...]
                            Which test statistics to use.
      -j NTHREADS, --nthreads NTHREADS
                            Number of parallel workers. Default: 1
      -r SNP_RANGE, --snp-range SNP_RANGE
                            Maximum allowable distance, in BP, between an exon and
                            a putative cis-sQTL. Default: 0
      -a ALPHA, --alpha ALPHA
                            Desired Type I error rate for FDR correction. Default:
                            0.05
      -p NPERMUTE, --npermute NPERMUTE
                            Number of permutations for permutation testing.
                            Default: 0
      -g MIN_GAP, --min-gap MIN_GAP
                            Minimum difference between two adjacent PSI values
                            required for consideration of each LSV. Default: 0.05
      -d MIN_DELTA_PSI, --min-delta-psi MIN_DELTA_PSI
                            Minimum difference in expected PSI between the two
                            homozygote classes required for consideration of each
                            LSV-SNP pair. Default: 0.1
      -k GAP_SELECT_K, --gap-select-k GAP_SELECT_K
                            Number of extreme PSI values to exclude when
                            performing PSI gap selection. Default: 3
      --extend-exons        If set, will consider putative cis-sQTLs within range
                            of any exon associated with the LSV, not just the
                            reference exon.
      --logit               If set, will logit transform PSI.
      --normalize {off,standard,quantile}
                            Normalization strategy. Choices: off, standard,
                            quantile. Default: off
      --two-stage           If set, correct and filter for best association per
                            gene, then perform a second-pass correction between
                            genes. Default: perform a single pass FDR correction
                            over all LSV-SNP pairs.
      --enable-gap-filtering
                            If set, filters the candidate set of LSVs based on
                            satisfying a minimum difference in adjacent PSI
                            values.
      --gwas GWAS           Path to GWAS linkage disequilibrium file.
      --min-ld MIN_LD       Minimum R2 correlation between SNPs tested for linkage
                            disequilibrium in --gwas. SNP pairs with worse
                            correlation will be discarded.
      --pre-lsv PRE_LSV     Set to a file path where preformatted LSVs are to be
                            stored. Creates this file if it does not exist.
      --contig-override     If set, will use the [contigs] section in the config
                            to map PSI chromosome IDs to VCF chromosome IDs.
      --debug               Extra debug prints
      --jn-sel-strategy {maxdpsi,maxtss}
                            Select the junction selection strategy. maxdpsi:
                            Select the junction with the greatest expected delta
                            PSI between homozygotes. maxtss: Select the junction
                            with the greatest total variance (total sum of
                            squares).
	  --dbsnp DBSNP         Reference VCF for dbSNP IDs

## Config file

### `info`

The following argument is required in this section:

`psidir` = path to where you can find all the input psi.voila files generated 
by MAJIQ.

### `psifiles`

Each line in this section represents the Voila output from one RNA-seq
experiment.  The label shall be the deidentified subject ID.  All paths are 
relative to the directory declared in `info/psidir`.

Example:

    GTEX-12345 = SRR133742069.psi.voila

### `contigs`

This section is meant to support the case where the chromosome identifiers in
the VCF do not exactly match the chromosome identifiers in the MAJIQ output.
Each key is a chromosome name as it appears in the MAJIQ annotation, and the
values are the corresponding names as they appear in the VCF.

Example:

    chr1 = 1

## Output

The output file is a tab-delimited text file with columns determined
dynamically from which statistics are specified in the config file.

### Constitutive columns

**lsv_id** - The ID of the LSV from MAJIQ.  This ID encodes the unique gene ID,
the coordinates of the reference exon, and whether the LSV is single-source or
single-target.

**jn_idx** - The index of the junction as reported in the MAJIQ meta data.

**snp_id** - The ID of the SNP from the VCF.

**distance** - The distance between the SNP and the splice siteprefer

**num_homo_aa** - The number of samples with genotype A/A for which the LSV was
quantifiable.

**epsi_homo_aa** - The expected PSI for genotype A/A.

**num_homo_ab** - The number of samples with genotype A/B for which the LSV was
quantifiable.

**epsi_homo_ab** - The expected PSI for genotype A/B.

**num_homo_bb** - The number of samples with genotype B/B for which the LSV was
quantifiable.

**epsi_homo_bb** - The expected PSI for genotype B/B.

### If `ftest`

**ftest_stat**, **ftest_pval**, and **ftest_fdr** are self-explanatory.

### If `ttest`

**ttest_aa_ab_stat**, **ttest_aa_ab_pval**, **test_aa_ab_fdr** are the T
statistic, pvalue, and corrected pvalue for the comparison between the A/A
and A/B genotype classes.  Similarly, **ttest_aa_bb_stat** etc. are for the
comparison between A/A and B/B, and **ttest_ab_bb_stat** etc. are for the
comparison between A/B and B/B.

### If `correl`

**slope** and **intercept** are the parameters of the least squares linear
regression of PSI onto genotype after logit transformation, quantile
normalization, and correction for covariates.  **correl_stat** is the Pearson
correlation statistic learned from the data, and **correl_pval** and
**correl_fdr** are the raw and corrected pvalue for this correlation.

### If `tnom`

TNOM attempts to split PSI values into three non-overlapping groups using two
thresholds such that when you assign genotype classes to each group, the total
number of mistakes (TNOM) resulting from this assignment is minimized.  This 
test is similar to a decision tree optimizing for training error.  
**tnom_tau1** and **tnom_tau2** are the two thresholds learned from the data 
that best split the PSI values with respect to the TNOM score (returned as 
**tnom_stat**). **tnom_pval** and **tnom_fdr** are the raw and corrected pvalue 
for the TNOM score.

**Developer's warning**: Take TNOM pvalue calculations with a grain of salt. 
They should not be used in downstream analysis at this time.

### If `kruskal`

**kruskal_stat**, **kruskal_pval**, and **kruskal_fdr** are self-explanatory.
