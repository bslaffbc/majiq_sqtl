import argparse
import csv
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st
from collections import defaultdict, Counter


def get_signif_assocs(fp, args):
    reader = csv.DictReader(fp, dialect='excel-tab')
    yield reader.fieldnames
    yield from filter(lambda line: float(line[args.key]) < args.alpha, reader)


def plot_correl(args):
    outpairs = defaultdict(list)
    counts = Counter()
    for name, fp in zip(args.names, args.infiles):
        reader = csv.DictReader(fp, dialect='excel-tab')
        for line in reader:
            counts[name] += float(line[args.key]) < args.alpha
            outpairs[name].append((line['lsv_id'], line['snp_id']))

    plt.figure(figsize=[10, 10])
    for i, name1 in enumerate(args.names[:-1]):
        n = counts[name1]
        v1 = outpairs[name1][:n]
        for name2 in args.names[i+1:]:
            print(name1, name2)
            v2 = outpairs[name2][:n]
            rr = [np.intersect1d(v1[:i], v2[:i]).size / (i + 1) for i in range(n + 1)]
            plt.plot(rr, label=f'{name1} vs {name2} (N = {n}, RR = {rr[-1] * 100:.02f}%)')
    plt.xlabel('Number of significant associations detected in first cell type')
    plt.ylabel('Fraction of associations recovered in second cell type')
    plt.legend(loc=0)
    plt.savefig(args.outfile, transparent=True)
    plt.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('outfile')
    parser.add_argument('infiles', nargs='+', type=argparse.FileType())
    parser.add_argument('--alpha', type=float, default=0.05)
    parser.add_argument('--key', default='linreg_fdr')
    parser.add_argument('--names', nargs='+', required=True)
    args = parser.parse_args()
    assert len(args.names) == len(args.infiles)
    plot_correl(args)


if __name__ == '__main__':
    main()
