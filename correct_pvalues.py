import argparse
import numpy as np
from src.pval_correction import correct_pvalues
from src.csv_io import get_dtype
from src.stats import statistics
import warnings


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile')
    parser.add_argument('-s', '--stats', nargs='+',
                        type=lambda s: statistics[s],
                        choices=list(statistics.keys()),
                        default=list(statistics.values()),
                        help='Which test statistics to use. '
                             'Choices: %(choices)s. '
                             'Default: %(default)s.')
    parser.add_argument('-a', '--alpha', type=float, default=0.05,
                        help='Desired Type I error rate for FDR correction. '
                             'Default: %(default).3g')
    parser.add_argument('-p', '--npermute', type=int, default=0,
                        help='Number of permutations for permutation testing. '
                             'Default: %(default)d')
    parser.add_argument('--two-stage', action='store_true',
                        help='If set, correct and filter for best association '
                             'per gene, then perform a second-pass correction '
                             'between genes. Default: perform a single pass '
                             'FDR correction over all LSV-SNP pairs.')
    parser.add_argument('--by-which', choices=['gene', 'lsv'], default='gene',
                        help='When doing two-stage correction, this sets '
                             'whether the correction is on the best '
                             'association per gene or the best association '
                             'per LSV. '
                             'Choices: %(choices)s. '
                             'Default: %(default)s')

    args = parser.parse_args()
    fmt, dtype = get_dtype(args)
    print(args.infile)

    with warnings.catch_warnings() as w:
        warnings.filterwarnings('error')
        outstats = np.genfromtxt(args.infile, dtype=dtype, delimiter='\t',
                                 encoding=None)

    for name, (_dtype, offset) in outstats.dtype.fields.items():
        if _dtype == np.object:
            outstats[name] = [x.decode() for x in outstats[name]]
    outstats = correct_pvalues(outstats, args)
    np.savetxt(args.infile, outstats, fmt=fmt, delimiter='\t',
               header='\t'.join([xx[0] for xx in dtype]))


if __name__ == '__main__':
    main()
