#!/bin/sh -x

. ${HOME}/MAJIQ_HET/bin/activate

fail () {
    >&2 echo $1
    exit 1
}

ANNOT=/data/DB/hg19/gencode.hg19.gff3
BUILD=/data/projects/sylvia_smc_ec/build_gencode_hg19
PSI=/data/projects/sylvia_smc_ec/psi_gencode_hg19
NTHREADS=16
BUILD_OPTS="-j ${NTHREADS}"
PSI_OPTS="-j ${NTHREADS}"
VOILA_OPTS="-j ${NTHREADS} --disable-html -s ${BUILD}/splicegraph.sql"
SQTL_OPTS="--splice-graph ${BUILD}/splicegraph.sql -v /data/sylvia_smc_ec/genotype/SMCEM.sanger.filtered.vcf.gz --contig-override -s linreg ftest --extend-exons -r 500 -c /data/sylvia_smc_ec/metadata/Covariates.txt --logit --normalize quantile"

if [ "$(majiq -v 2> /dev/null)" != "1.1.3a" ]; then
    pip install -U pip setuptools || fail "Unable to install packages pip, setuptools"
    pip install -U cython numpy scipy || fail "Unable to install packages cython, numpy, scipy"
    pip install -U git+https://bitbucket.org/biociphers/majiq_stable#egg=majiq || fail "Unable to install package majiq"
fi

majiq build -c smc_ec.ini -o ${BUILD} ${ANNOT} ${BUILD_OPTS} || fail "MAJIQ builder failed"

for sample in ${BUILD}/*.majiq; do
    bname=$(basename ${sample%.*})
    majiq psi -o ${PSI} -n ${bname} ${sample} ${PSI_OPTS} || fail "MAJIQ PSI for ${bname} failed"
    voila psi -o ${PSI} psi_gencode_hg19/${bname}.psi.voila ${VOILA_OPTS} || fail "VOILA PSI for ${bname} failed"
done

python ${HOME}/majiq_sqtl/scripts/gen_config.py HCASMC ./smc_ec.ini.in ${PSI} smc_gencode.ini || fail "Failed to create config for SMC"
python ${HOME}/majiq_sqtl/scripts/gen_config.py HCAEC ./smc_ec.ini.in ${PSI} ec_gencode.ini || fail "Failed to create config for EC"

python ${HOME}/majiq_sqtl/call_sqtls.py smc_gencode.ini smc_gencode.genomewide.tsv --pre-lsv smc_gencode.pkl ${SQTL_OPTS} || fail "Failed to call sQTLs for SMC"
python ${HOME}/majiq_sqtl/call_sqtls.py ec_gencode.ini ec_gencode.genomewide.tsv --pre-lsv ec_gencode.pkl ${SQTL_OPTS} || fail "Failed to call sQTLs for EC"
